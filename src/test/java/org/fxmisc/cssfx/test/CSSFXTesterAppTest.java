package org.fxmisc.cssfx.test;

import org.fxmisc.cssfx.test.ui.AbstractTestableGUITest;
import org.junit.Before;
import org.junit.Test;
import org.loadui.testfx.utils.FXTestUtils;

public class CSSFXTesterAppTest extends AbstractTestableGUITest<CSSFXTesterApp> {
    public CSSFXTesterAppTest() {
        super(CSSFXTesterApp.class);
    }
    
    @Before
    public void init() throws Exception {
        if (stage != null) {
            FXTestUtils.invokeAndWait(() -> getTestedInstance().initUI(stage), 5);
        }
    }
    
    @Test
    public void canLoadTheApplication() {
        click("#dynamicBar");
        click("#dynamicCSS");
        click("#dynamicStage");
        
        sleep(100);
    }
}
