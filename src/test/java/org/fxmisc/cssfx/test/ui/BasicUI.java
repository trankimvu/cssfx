package org.fxmisc.cssfx.test.ui;


import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.fxmisc.cssfx.CSSFX;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class BasicUI extends Application implements TestableUI {
    @Override
    public Parent getRootNode() {
        VBox container = new VBox();
        container.getStyleClass().add("container");
        
        Label lblWelcome = new Label("Welcome");
        Label lblCSSFX = new Label("CSSFX");
        lblCSSFX.setId("cssfx");
    
        container.getChildren().addAll(lblWelcome, lblCSSFX);
        
        String basicURI = BasicUI.class.getResource("basic.css").toExternalForm();
        container.getStylesheets().add(basicURI);
        
        return container;
    }
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent p = getRootNode();
        Scene s = new Scene(p, 800, 600);
        stage.setScene(s);
        stage.show();
        
        // The CSS used by the UI
        URI basicCSS = BasicUI.class.getResource("basic.css").toURI();
        // a resource containing the required changes we want to apply to the CSS 
        URI changedBasicCSS = BasicUI.class.getResource("basic-cssfx.css").toURI();
        
        // start CSSFX
        Runnable stopper = CSSFX.onlyFor(p)
                .noDefaultConverters()
                .addConverter((uri) -> {
                    try {
                        if (basicCSS.toURL().toExternalForm().equals(uri)) {
                            return Paths.get(changedBasicCSS);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }).start();

    }
}
