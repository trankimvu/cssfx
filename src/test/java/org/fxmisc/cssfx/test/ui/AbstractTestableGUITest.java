package org.fxmisc.cssfx.test.ui;

import javafx.scene.Parent;

import org.loadui.testfx.GuiTest;

public abstract class AbstractTestableGUITest<T extends TestableUI> extends GuiTest {
    private Class<T> uiClass;
    private Parent rootNode;
    private T testedInstance;

    protected AbstractTestableGUITest(Class<T> testableClass) {
        uiClass = testableClass;
    }
    
    protected T getTestedInstance() {
        return testedInstance;
    }

    @Override
    protected Parent getRootNode() {
        try {
            testedInstance = uiClass.newInstance();
            rootNode = testedInstance.getRootNode();
            return rootNode;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    protected Parent builtRootNode() {
        return rootNode;
    }
}
