package org.fxmisc.cssfx.test.ui;

import javafx.scene.Parent;

public interface TestableUI {
    public Parent getRootNode();
}
