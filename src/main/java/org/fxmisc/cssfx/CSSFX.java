package org.fxmisc.cssfx;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.fxmisc.cssfx.api.URIToPathConverter;
import org.fxmisc.cssfx.impl.URIToPathConverters;

public class CSSFX {
    /**
     * Directly start monitoring the CSS of the application using defaults:
     * <ul>
     * <li>standard source file detectors: Maven, Gradle, execution from built JAR (details in {@link URIToPathConverters#DEFAULT_CONVERTERS})</li>
     * <li>detection activated on all stages of the application, including the ones that will appear later on</li>
     * </ul> 
     * @return a Runnable object to stop CSSFX monitoring
     */
    public static Runnable start() {
        return new CSSFXConfig().start();
    }
    
    /**
     * Directly start monitoring CSS for the given Stage.
     * <ul>
     * <li>standard source file detectors: Maven, Gradle, execution from built JAR (details in {@link URIToPathConverters#DEFAULT_CONVERTERS})</li>
     * <li>detection activated on the stage only (and its children)</li>
     * </ul> 
     * @param stage the stage that will be monitored
     * @return a Runnable object to stop CSSFX monitoring
     */
    public static Runnable start(Stage stage) {
        CSSFXConfig cfg = new CSSFXConfig();
        cfg.setRestrictedToStage(stage);
        return cfg.start();
    }
    
    /**
     * Directly start monitoring CSS for the given Scene.
     * <ul>
     * <li>standard source file detectors: Maven, Gradle, execution from built JAR (details in {@link URIToPathConverters#DEFAULT_CONVERTERS})</li>
     * <li>detection activated on the scene only (and its children)</li>
     * </ul> 
     * @param scene the scene that will be monitored
     * @return a Runnable object to stop CSSFX monitoring
     */
    public static Runnable start(Scene scene) {
        CSSFXConfig cfg = new CSSFXConfig();
        cfg.setRestrictedToScene(scene);
        return cfg.start();
    }
    
    /**
     * Directly start monitoring CSS for the given node.
     * <ul>
     * <li>standard source file detectors: Maven, Gradle, execution from built JAR (details in {@link URIToPathConverters#DEFAULT_CONVERTERS})</li>
     * <li>detection activated on the node only (and its children)</li>
     * </ul> 
     * @param node the node that will be monitored
     * @return a Runnable object to stop CSSFX monitoring
     */
    public static Runnable start(Node node) {
        CSSFXConfig cfg = new CSSFXConfig();
        cfg.setRestrictedToNode(node);
        return cfg.start();
    }

    /**
     * Restrict the source file detection for graphical sub-tree of the given {@link Stage}
     * 
     * @param s the stage to restrict the detection on, if null then no restriction will apply
     * @return a {@link CSSFXConfig} object as a builder to allow further configuration
     */
    public static CSSFXConfig onlyFor(Stage s) {
        CSSFXConfig cfg = new CSSFXConfig();
        cfg.setRestrictedToStage(s);
        return cfg;
    }
    /**
     * Restrict the source file detection for graphical sub-tree of the given {@link Scene}
     * 
     * @param s the scene to restrict the detection on, if null then no restriction will apply
     * @return a {@link CSSFXConfig} object as a builder to allow further configuration
     */
    public static CSSFXConfig onlyFor(Scene s) {
        CSSFXConfig cfg = new CSSFXConfig();
        cfg.setRestrictedToScene(s);
        return cfg;
    }
    /**
     * Restrict the source file detection for graphical sub-tree of the given {@link Node}
     * 
     * @param n the node to restrict the detection on, if null then no restriction will apply
     * @return a {@link CSSFXConfig} object as a builder to allow further configuration
     */
    public static CSSFXConfig onlyFor(Node n) {
        CSSFXConfig cfg = new CSSFXConfig();
        cfg.setRestrictedToNode(n);
        return cfg;
    }

    /**
     * Register a new converter that will be used to map CSS resources to local file.
     * @param converter an additional converter to use, ignored if null
     * @return a {@link CSSFXConfig} object as a builder to allow further configuration
     */
    public static CSSFXConfig addConverter(URIToPathConverter converter) {
        return new CSSFXConfig().addConverter(converter);
    }

}
