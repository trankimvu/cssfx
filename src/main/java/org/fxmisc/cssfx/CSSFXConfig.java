package org.fxmisc.cssfx;

import com.sun.javafx.stage.StageHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.fxmisc.cssfx.api.URIToPathConverter;
import org.fxmisc.cssfx.impl.CSSFXMonitor;
import org.fxmisc.cssfx.impl.URIToPathConverters;
import org.fxmisc.cssfx.impl.log.CSSFXLogger;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Stores information before finally building/starting the CSS monitoring.
 *
 * @author Matthieu Brouillard
 */
public class CSSFXConfig {
    // LinkedHashSet will preserve ordering
    private final Set<URIToPathConverter> converters = new LinkedHashSet<URIToPathConverter>(Arrays.asList(URIToPathConverters.DEFAULT_CONVERTERS));
    private Stage restrictedToStage = null;
    private Scene restrictedToScene = null;
    private Node restrictedToNode = null;

    CSSFXConfig() {
    }

    void setRestrictedToStage(Stage restrictedToStage) {
        this.restrictedToStage = restrictedToStage;
    }

    void setRestrictedToScene(Scene restrictedToScene) {
        this.restrictedToScene = restrictedToScene;
    }

    void setRestrictedToNode(Node restrictedToNode) {
        this.restrictedToNode = restrictedToNode;
    }

    /**
     * Empty the list of default converters.
     * Especially usefull for testing purposes where full control of the converters is required.
     * @return a {@link CSSFXConfig} object as a builder to allow further configuration
     */
    public CSSFXConfig noDefaultConverters() {
        converters.clear();
        return this;
    }

    /**
     * Register a new converter that will be used to map CSS resources to local file.
     * @param converter an additional converter to use, ignored if null
     * @return a {@link CSSFXConfig} object as a builder to allow further configuration
     */
    public CSSFXConfig addConverter(URIToPathConverter converter) {
        converters.add(converter);
        return this;
    }

    /**
     * Start monitoring CSS resources with the config parameters collected until now.
     * @return a Runnable object to stop CSSFX monitoring
     */
    public Runnable start() {
        if (!CSSFXLogger.isInitialized()) {
            if (Boolean.getBoolean("cssfx.log")) {
                CSSFXLogger.LogLevel toActivate = CSSFXLogger.LogLevel.INFO;
                String levelStr = System.getProperty("cssfx.log.level", "INFO");
                try {
                    toActivate = CSSFXLogger.LogLevel.valueOf(levelStr);
                } catch (Exception ignore) {
                    System.err.println("[CSSFX] invalid value for cssfx.log.level, '" + levelStr + "' is not allowed. Select one in: " + Arrays.asList(CSSFXLogger.LogLevel.values()));
                }
                CSSFXLogger.setLogLevel(toActivate);

                String logType = System.getProperty("cssfx.log.type", "console");
                switch (logType) {
                case "noop":
                    CSSFXLogger.noop();
                    break;
                case "console":
                    CSSFXLogger.console();
                    break;
                case "jul":
                    CSSFXLogger.jul();
                    break;
                default:
                    System.err.println("[CSSFX] invalid value for cssfx.log.type, '" + logType + "' is not allowed. Select one in: " + Arrays.asList("noop", "console", "jul"));
                    break;
                }
            } else {
                CSSFXLogger.noop();
            }
        }

        CSSFXMonitor m = new CSSFXMonitor();

        if (restrictedToStage != null) {
            m.setStages(FXCollections.singletonObservableList(restrictedToStage));
        } else if (restrictedToScene != null) {
            m.setScenes(FXCollections.singletonObservableList(restrictedToScene));
        } else if (restrictedToNode != null) {
            m.setNodes(FXCollections.singletonObservableList(restrictedToNode));
        } else {
            // we monitor all the stages
            ObservableList<Stage> monitoredStages = (restrictedToStage == null)? StageHelper.getStages():FXCollections.singletonObservableList(restrictedToStage);
            m.setStages(monitoredStages);
        }

        return start(() -> m);
    }

    private Runnable start(Callable<CSSFXMonitor> monitorBuilder) {
        CSSFXMonitor mon;
        try {
            mon = monitorBuilder.call();
            mon.addAllConverters(converters);
            mon.start();
            return mon::stop;
        } catch (Exception e) {
            throw new RuntimeException("could not create CSSFXMonitor", e);
        }
    }
}
